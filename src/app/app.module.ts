import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { LogModule } from '@dagonmetric/ng-log';
import { GTagLoggerModule } from '@dagonmetric/ng-log-gtag';
import { ConsoleLoggerModule } from '@dagonmetric/ng-log/console';

import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,

        LogModule,
        ConsoleLoggerModule,
        GTagLoggerModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
